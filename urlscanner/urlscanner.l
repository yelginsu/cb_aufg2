%option noyywrap
%option yylineno
%option nounput
%option nodefault
%{
#include "urlscanner.h"
%}

%x BEGIN_A_TAG
%x END_OF_A_TAG
%x HREF
%x TEXT

WHITE_SPACE                           [ \t\n\r]
URL_CHAR                        [a-zA-Z0-9\/.:]
%%


<INITIAL><a                     {BEGIN(BEGIN_A_TAG);}
<BEGIN_A_TAG>href=\"           {BEGIN(HREF);}
<BEGIN_A_TAG>.|\n              {}

<HREF>\"                        {BEGIN(END_OF_A_TAG);}
<HREF>[^\"]*                    {yylval = yytext; return TOKEN_URL;}
<END_OF_A_TAG>>                  {BEGIN(TEXT);}
<END_OF_A_TAG>.|\n               {}

<TEXT><\/a({WHITE_SPACE}*)>           {BEGIN(INITIAL);}
<TEXT>[^<]*                     {yylval = yytext; return TOKEN_TEXT;}
<INITIAL>.|\n                   {}

<<EOF>>                         {return MYEOF;}
%%
