%option noyywrap
%option yylineno
%option nounput
%option noinput
%option nodefault

%{
#include "minako.h"
#include <math.h>
#include <string.h>
%}

%x COMMENT1
%x COMMENT2

DIGIT		[0-9]
INTEGER		{DIGIT}+
FLOAT		{INTEGER}"."{INTEGER}|"."{INTEGER}
BOOLEAN		"true"|"false"
LETTER 		[a-zA-Z]
ID 			({LETTER})+({DIGIT}|{LETTER})*
STRING		"\""[^\n\"]*"\""

whitespace  [ \t\n]

%%

{whitespace}+                      	{  }

"/*"								{ BEGIN(COMMENT1); }
"//"								{ BEGIN(COMMENT2); }	
						  
"&&"								{ return AND; }
"||"								{ return OR; }
"=="								{ return EQ; }
"!="								{ return NEQ; }
"<="								{ return LEQ; }
">="								{ return GEQ; }
"<"									{ return LSS; }
">"									{ return GRT; }

"bool"								{ return KW_BOOLEAN; }
"do"								{ return KW_DO; }
"else"								{ return KW_ELSE; }
"float"								{ return KW_FLOAT; }
"for"								{ return KW_FOR; }
"if"								{ return KW_IF; }
"int"								{ return KW_INT; }
"printf"							{ return KW_PRINTF; }
"return"							{ return KW_RETURN; }
"void"								{ return KW_VOID; }
"while"								{ return KW_WHILE; }

"+"|"-"|"*"|"/"|"="					{ return yytext[0]; }
","|";"|"("|")"|"{"|"}"				{ return yytext[0]; }

{INTEGER}							{ yylval.intValue = atoi(yytext); return CONST_INT; }
{FLOAT}								{ yylval.floatValue = atof(yytext); return CONST_FLOAT; }
{BOOLEAN}							{ yylval.intValue=strcmp(yytext,"false"); return CONST_BOOLEAN; }
{STRING} 							{ yylval.string=(char*)malloc(sizeof(char)*(strlen(yytext)+1)); strcpy(yylval.string,yytext); return CONST_STRING; }
{ID} 								{ yylval.string=(char*)malloc(sizeof(char)*(strlen(yytext)+1)); strcpy(yylval.string,yytext); return ID; }


<<EOF>>								{ return EOF; }
.									{ printf("Oops lexical error in line: %d\n", yylineno); exit(42); }


<COMMENT1>"*/"                      { BEGIN(INITIAL); }
<COMMENT1>.|\n						{  }

<COMMENT2>\n						{ BEGIN(INITIAL); }
<COMMENT2>.							{  }

%%
